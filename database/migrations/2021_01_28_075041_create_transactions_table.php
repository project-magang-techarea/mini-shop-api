<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('member_id')->unsigned()->nullable();
            $table->string('trx_number');
            $table->integer('quantity');
            $table->bigInteger('discount_id')->unsigned()->nullable();
            $table->integer('total');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('discount_id')->references('id')->on('discounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
